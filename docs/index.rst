.. sample_django_two_scoops documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sample_django_two_scoops's documentation!
====================================================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   deploy
   docker_ec2
   tests



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
